(function ($, Drupal) {
  Drupal.behaviors.simple_toc_block = {
  attach: function (context, settings) {
  
    // Automatically create bookmarks to headers on the page
    $('.simple-toc-block-placeholder', context).once('generate-simple-toc-block').each(function () {
      CreateHeaderBookmarks();
    });
  
  
    // Automatically create bookmarks to headers on the page
    function CreateHeaderBookmarks() {
      //var root_level = 0;
      var current_level = 0;
      //var previous_level = 0;
   
      // Determine the header levels to include in the table of contents
      var header_level_min = (typeof settings.header_level_min == 'undefined' ? 1 : parseInt(settings.header_level_min));
      var header_level_max = (typeof settings.header_level_max == 'undefined' ? 1 : parseInt(settings.header_level_max));
      var header_level_selector_string = '';
      for (var i = Math.min(header_level_min, header_level_max); i <= Math.max(header_level_min, header_level_max); i++) {
        header_level_selector_string += 'h' + i.toString() + ', ';
      }
      header_level_selector_string = header_level_selector_string.substring(0, header_level_selector_string.length - 2);
  
      $('.simple-toc-block-placeholder').each(function() {
        // Determine this block's region
        var uniqueid = $(this).data('uniqueid');
        var classes = $(this).parents(".region").attr('class').split(' ');
        var region = classes.filter(function(item, index) {
          return item.indexOf('region-') == 0;
        });
  
        // Generate the bookmarks
        if ($('#simple-toc-block-' + uniqueid).has('fielset ul.bookmark-list')) {
          var bookmark_html = '';
          $('.' + region).find(header_level_selector_string).not('.visually-hidden, .title.page-title, .block-simple-toc-block .title').each(function() {
            // Determine the current level
            current_level = $(this).prop('tagName').toLowerCase().replace('h', '');
            //if (root_level == 0) root_level = current_level;
  
            // Determine the ID
            var header_id = $(this).text();
            header_id = header_id.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
  
            // Assign the ID
            $(this).attr({
              id: header_id,
              name: header_id,
            });
  
            // Add the bookmark for the ID
            var bookmark_link_html = '<a href="#' + header_id + '">' + $(this).text() + '</a>';
            //if (current_level == root_level)
            bookmark_html += '<li class="header-level-' + current_level + '">' + bookmark_link_html + '</li>';
            //else if (current_level > previous_level)
            //	bookmark_html += "<li><ul><li>" + bookmark_link_html + "</li></ul></li>";
          });
  
          // Add the bookmarks html to the DOM
          if (bookmark_html != '') $('#simple-toc-block-' + uniqueid).html('<fieldset class="simple-toc-block"><legend>On this page:</legend><ul class="bookmark-list">' + bookmark_html + '</ul></fieldset>');
  
          // Remember that this block now has bookmarks
          $('#simple-toc-block-' + uniqueid).addClass('bookmarks-added');
        }
      });
    }
  
  }
  };
  })(jQuery, Drupal);