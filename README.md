## CONTENTS OF THIS FILE ##

 * Introduction
 * Installation
 * Configuration
 * Permissions
 * How Can You Contribute?
 * Maintainers

## INTRODUCTION ##

Author and maintainer: Ryan Kavalsky (ryankavalsky)
 * Drupal: https://www.drupal.org/u/ryankavalsky
 * Personal: https://ryankavalsky.com/

This module will allow you to add a Table of Contents anywhere on your site, using Drupal's standard Block Layout tool. It simply looks for any HTML header (h1, h2, etc) within that block's same region, and generates a Table of Contents with links to each header.

## INSTALLATION ##

See https://www.drupal.org/documentation/install/modules-themes/modules-8
for instructions on how to install or update Drupal modules.

## CONFIGURATION ##

### PERMISSIONS ###

Any user with the 'administer blocks' permission can place a TOC block.

## HOW CAN YOU CONTRIBUTE? ##

 * Report any bugs, feature or support requests in the issue tracker; if
   possible help out by submitting patches.
   http://drupal.org/project/issues/simple_toc_block

 * Do you know a non-English language? Help to translate the module.
   https://localize.drupal.org/translate/projects/simple_toc_block

 * If you would like to say thanks and support the development of this module, a
   donation will be much appreciated.
   https://www.paypal.com/donate/?business=RVHDRAEJ9CFB2&no_recurring=0&item_name=Simple+TOC+Block+module+development&currency_code=USD

 * Feel free to contact me for paid support: https://ryankavalsky.com/contact/connect

## MAINTAINERS ##

Current maintainers:
 * Ryan Kavalsky (ryankavalsky) - https://www.drupal.org/u/ryankavalsky
