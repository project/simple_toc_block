<?php

namespace Drupal\simple_toc_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for Simple TOC Block.
 */
class SimpleTOCBlockSettingsForm extends ConfigFormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_toc_block_settings';
  }  


  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'simple_toc_block.settings',
    ];
  }  


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('simple_toc_block.settings');

    $form['header_level_min'] = [
      '#title' => $this->t('Highest HTML Header Level to Include'),
      '#type' => 'number',
      '#min' => 1,
      '#max' => 10,
      '#default_value' => ($config->get('header_level_min') ?? '1'),
      '#description' => 'Provide the highest header level to include in the table of contents.  For example: Entering "1" would include headers starting at the h1 level.'
    ];

    $form['header_level_max'] = [
      '#title' => $this->t('Deepest HTML Header Level to Include'),
      '#type' => 'number',
      '#min' => 1,
      '#max' => 10,
      '#default_value' => ($config->get('header_level_max') ?? '1'),
      '#description' => 'Provide the deepest header level to include in the table of contents.  For example: Entering "3" would include headers down to the h3 level.'
    ];

    return parent::buildForm($form, $form_state);
  }  


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::configFactory()->getEditable('simple_toc_block.settings')
      ->set('header_level_min', $form_state->getValue('header_level_min'))
      ->set('header_level_max', $form_state->getValue('header_level_max'))
      ->save();

    parent::submitForm($form, $form_state);
  }  


  /**
   * {@inheritdoc}
   */
  public function ShowConfigurationPage() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('<h1>Simple TOC Block Configuration</h1>'),
    ];
  }

}
