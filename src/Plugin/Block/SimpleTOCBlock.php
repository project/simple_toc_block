<?php

namespace Drupal\simple_toc_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SimpleTOCBlock' block.
 *
 * @Block(
 *   id = "simple_toc_block",
 *   admin_label = @Translation("Simple TOC Block"),
 *   category = @Translation("Simple TOC Block"),
 * )
 */
class SimpleTOCBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // Determine the tags to include in the TOC
    $config = \Drupal::config('simple_toc_block.settings');
    $header_level_min = $config->get('header_level_min');
    $header_level_max = $config->get('header_level_max');

    $unique_id = uniqid();
    $build = [
      '#type' => 'markup',
      '#markup' => '<span id="simple-toc-block-' . $unique_id . '" class="simple-toc-block-placeholder" data-uniqueid="' . $unique_id . '"></span>',
      '#attached' => [
        'library' => ['simple_toc_block/simple_toc_block'],
        'drupalSettings' => [
          'header_level_min' => $header_level_min,
          'header_level_max' => $header_level_max,
        ],
      ],
      '#cache' => [
        'contexts' => ['url'],
      ],
    ];

    return $build;
  }

}
